# Pharos quick start

## Requirements

* existing Kubernetes (k8s) service with the following:
  * internal DNS provider
  * Ingress controller
  * Role Based Access Control (RBAC)
  * default Storage Class for persistent volumes
* Workstation with the following:
  * `kubectl` client configured to control your k8s service
  * `helm` client

## Helm client configuration

```bash
# Add the Pharos helm chart repository
helm repo add pharos https://bitbucket.org/rherrick/pharos-charts
helm repo update
```

## Deploy Pharos

```bash
# Create a namespace and deploy the Pharos service
kubectl create namespace pharos
helm upgrade pharos pharos/pharos --install --values ./my-site-overrides.yaml --namespace pharos

# Watch the Pharos goodness
watch kubectl -npharos get all
```

## Parameters

The following tables list the configuration parameters of the Pharos Helm chart and their default values.

| Parameter                                   | Description                                                                          | Default |
| ------------------------------------------- | ------------------------------------------------------------------------------------ | --- |
| `global.imageRegistry`                      | Global Docker Image registry                                                         | `nil` |
| `global.imagePullSecrets`                   | Global Docker registry secret names as an array                                      | `[]` (does not add image pull secrets to deployed pods) |
| `global.postgresql.postgresqlDatabase`      | PostgreSQL database (overrides `postgresql.postgresqlDatabase`)                      | `nil` |
| `global.postgresql.postgresqlUsername`      | PostgreSQL username (overrides `postgresql.postgresqlUsername`)                      | `nil` |
| `global.postgresql.postgresqlPassword`      | PostgreSQL admin password (overrides `postgresql.postgresqlPassword`)                | `nil` |
| `global.postgresql.servicePort`             | PostgreSQL port (overrides `postgresql.service.port`)                                | `nil` |
| For more PostgreSQL detail and configuration options please visit the official [Bitnami Chart repository](https://github.com/bitnami/charts/tree/master/bitnami/postgresql). |||
| `global.storageClass`                       | Global storage class for dynamic provisioning                                        | `nil` |
| `volumes.archive.existingClaim`    | | |
| `volumes.prearchive.existingClaim` | | |

